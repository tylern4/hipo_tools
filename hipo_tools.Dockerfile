FROM rootproject/root:6.30.02-alma9

ENV HIPO_TOOLS=/usr/local/hipo_tools
ENV PATH=$PATH:$HIPO_TOOLS/bin
ENV LD_LIBRARY_PATH=$HIPO_TOOLS:/opt/conda/lib

COPY . /source
WORKDIR /source
RUN export CC=$(root-config --cc) && \
	export CXX=$(root-config --cxx) && \
	mkdir build && cd build && \
	cmake .. -DCMAKE_INSTALL_PREFIX=${HIPO_TOOLS} && \
	make -j 8 install

